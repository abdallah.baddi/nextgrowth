let users = [
    {
      id: "123456789",
      createdDate: "2021-01-06T00:00:00.000Z",
      status: "En validation",
      firstName: "Mohamed",
      lastName: "Taha",
      userName: "mtaha",
      registrationNumber: "2584",
    },
     {
      id: "987654321",
      createdDate: "2021-07-25T00:00:00.000Z",
      status: "Validé",
      firstName: "Hamid",
      lastName: "Orrich",
      userName: "horrich",
      registrationNumber: "1594",
    },
    {
      id: "852963741",
      createdDate: "2021-09-15T00:00:00.000Z",
      status: "Rejeté",
      firstName: "Rachid",
      lastName: "Mahidi",
      userName: "rmahidi",
      registrationNumber: "3576",
    }
 ]

//Tool to convert ISO string date to fomat dd/mm/yyyy
function toolDate(isoDate){
    let date = new Date(isoDate);
    return date.getDate()+'/'+ (date.getMonth()+1) + '/'+date.getFullYear();
}

//Manage status color
function statusColor(value){
  if(value === 'Validé'){
    return 'valide'
  }else if(value === 'Rejeté'){
    return 'rejected'
  }else{
    return 'on-validation'
  }
}

// Function to map Users in the View
function loadUsers(users){
    let tableBody = document.getElementById('user-body');
    let html = '';
    users.forEach(user => {
        html += `<tr><td>${user.id}</td>`
        html += `<td>${toolDate(user.createdDate)}</td>`;
        html += `<td><span class="${statusColor(user.status)}">${user.status}</span></td>`;
        html += `<td>${user.lastName}</td>`;
        html += `<td>${user.firstName}</td>`;
        html += `<td>${user.userName}</td>`;
        html += `<td>${user.registrationNumber}</td>`;
        html += `<td><a href="" onClick="return deleteUser('${user.id}')"><img src="delete.png" class="icon"></a></td></tr>`;
    });
    tableBody.innerHTML = html;
}

//Function to remove user
function deleteUser(id){
    users = users.filter((user)=>{
       return user.id !== id
    });
    loadUsers(users);
    return false;
}

//Open Modal function
function openModalAddUser(){
  document.getElementById("open-modal").addEventListener("click", function(event){
    event.preventDefault();
    document.getElementById("wrap-modal").classList.remove("hide");
  });
}

//Close Modal function
function closeModalAddUser(){
  document.getElementById("wrap-modal").classList.add("hide");
}

// Add User function
function addUser(){
  document.getElementById("add-user").addEventListener("click", function(event){
    event.preventDefault();

    let data = new FormData(document.querySelector('form'));
    let user = {};
    //Serialize Form
    for (let [key, value] of data) {
      if(key === 'createdDate'){
        dateArr = value.split("-");
        let dobj = new Date(parseInt(dateArr[0]),parseInt(dateArr[1])-1,parseInt(dateArr[2]));
        user[key] = dobj.toISOString();
      }
      else{
        user[key] = value;
      }
      
    }
    //ID Creation
    let id = Math.floor(Math.random() * 100000000) + 900000000;
    user.id = id.toString();
    //Reset Form
    document.getElementById("user-form").reset();
    //Update data
    users.push(user);
    loadUsers(users)
    //Close Modal
    closeModalAddUser();
  });
}

//Close modal if we click on the black background
document.getElementById("cover").addEventListener("click", function(event){
  closeModalAddUser();
});

loadUsers(users);
openModalAddUser();
addUser();